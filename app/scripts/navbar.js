function main () {
  // const navButton = $('#navigation-button');
  const navButton = document.getElementById('navigation-button')
  const menuIcon              = $('#menu-icon')
  const closeIcon             = $('#close-icon')
  const navList               = $('#navigation')
  const navbar    = document.querySelector('nav')

  function toggleNavButtonIcon () {
    console.log('toggleButtonIcon...')
  }

  function toggleNavButtonAction (event) {
    let eventTarget = event
    if (eventTarget.getAttribute('aria-expanded') === 'true') {
      eventTarget.setAttribute('aria-expanded', 'false')
    } else {
      eventTarget.setAttribute('aria-expanded', 'true')
    }

    navList.slideToggle('fast')
  }

  navButton.addEventListener('click', function () {
    toggleNavButtonAction(this)
  })
}

$(document).ready(function () {
  main()
})
